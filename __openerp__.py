{
    'name': 'Trip Management',
    'version': '0.1',
    'category': 'Fleet Management',
    'sequence': 14,
    'summary': 'Fleet Management',
    'description': """
        Fleet Management

    """,
    'author': 'PYSolutions',
    'website': ' www.pysolutions.com',
    'images': [],
    'depends': ['fleet', 'product', 'sale','hr',],
    'data': [
        'wizard/update_location_view.xml',
        'trip_view.xml',
        'vehicle_view.xml',
        'fleet_data.xml',
    ],
    'demo': [],
    'test': [],
    "update_xml" : [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
