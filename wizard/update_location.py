from osv import fields, osv

class UpdateLocation(osv.osv_memory):
    _name = "fleet.trip.update_location"
    _columns = {
        'current_location': fields.many2one('fleet.location', 
            'Current Location', required=True),
    }
    
    def update_location(self, cursor, user, ids, context=None):
        print context
        obj = self.pool.get(context['active_model'])
        vehicle_obj = self.pool.get('fleet.vehicle')
        trips = obj.browse(cursor, user, context['active_ids'], context=context)
        data = self.browse(cursor, user, ids, context=context)[0]
        vehicle_ids = []
        for trip in trips:
            vehicle_ids.append(trip.truck.id)
            vehicle_ids.append(trip.trailer.id)
            vehicle_obj.write(cursor, user, vehicle_ids, 
                {'location': data.current_location.id}, 
                context=context)
        return True
