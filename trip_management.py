from openerp.osv import fields, osv
import datetime

class TripManagement(osv.osv):
    _name = 'fleet.trip'
    _description = 'trip management'

    _columns = {
        'contract': fields.many2one('sale.order', 'Contract', required=True),
        'driver_id': fields.many2one('hr.employee', 'Driver', required=True,
                                     help='Driver of the vehicle'),
        'courier_id': fields.many2one('hr.employee', 'Courier',
                                 help='Courier/Helper/Cleaner of the vehicle'),
        'trip_name': fields.char('Trip Name', required=True),
        'truck': fields.many2one('fleet.vehicle', 'Truck', required=True, domain=[('type','=','truck')]),
        'trailer': fields.many2one('fleet.vehicle','Trailer', required=True, domain=[('type','=','trailer')]),
        'start_point': fields.many2one('fleet.location', 'Start Point'),
        'end_point': fields.many2one('fleet.location', 'End Point'),
        'route': fields.many2one('fleet.route', 'Route'),
        'conditions': fields.text('Conditions'),
        'net_distance': fields.float('Net Distance'),
        'distane_unit': fields.selection([
                                          ('kilometers', 'Kilometers'),
                                          ('miles','Miles')
                                         ], 'Unit'),
        'quality_of_goods': fields.text('Nature and Quality of Goods'),
        'start_date': fields.datetime('Start Date'),
        'end_date': fields.datetime('End Date'),
        'last_stop': fields.many2one('fleet.location', 'Last Stop'),
        'state': fields.selection([
                                  ('draft', 'Draft'),
                                  ('confirm','Confirm'),
                                  ('inprocess','In Process'),
                                  ('breakdown','Break Down'),
                                  ('complete','Complete'),
                                  ('cancel','Cancel'),
                                  ], 'State'),
        'departure_place': fields.many2one('fleet.location', 'Departure Place'),
        'expected_departure_to_loading': fields.datetime('Expected ' 
                                                        'Departure To Loading'),
        'expected_departure_to_departure': fields.datetime('Expected ' 
                                                    'Departure To Destination'
                                                        ),
        'estimated_end_date': fields.datetime('Estimated End Date'),
        'price': fields.integer('Price'),
        'next_trip_date': fields.datetime('Next Trip Date'),

        #Loading & Unloading
        'loading_place': fields.many2one('fleet.location', 'Loading Place'),
        'loading_start': fields.datetime('Loading Start'),
        'loading_end': fields.datetime('Loading End'),
        'unloading_place': fields.many2one('fleet.location', 'Unloading Place'),
        'unloading_start': fields.datetime('Unloading Start'),
        'unloading_end': fields.datetime('Unloading End'),
        'destination_place': fields.many2one('fleet.location', 'Destination Place'),
        'arrival_loading': fields.datetime('Arrival Loading'),

        #Accounts
        'invoice_type': fields.many2one('trip.invoice.type', 'Invoice Type'),
        'invoice_number': fields.many2one('account.invoice', 'Invoice'),
	    'advance_payment': fields.integer('Advance Payment'),
	    'advance_date': fields.date('Advance Date'),
	    'account_advance': fields.integer('Account Advance'),
	    'balance_payment': fields.integer('Balance Payment'),
	    'balance_date': fields.date('Balance Date'),
	    'account_balance': fields.float('Account Balance'),
	    'dispatch_order': fields.integer('Dispatch Order'),
	    'amount_commission': fields.integer('Amount Commission'),
        'additional_trip_costs': fields.integer('Additional Trip Costs'),
        'invoiced': fields.boolean('Invoiced'),
        
        #Others
	    'driver_paycut': fields.float('Driver Paycut'),
        'sender_name': fields.many2one('res.partner', 'Sender Name'),
        'receiver_name': fields.many2one('res.partner', 'Receiver Name'),
        'customs_instructions': fields.text('Customs Instructions'),
        'gps_truck': fields.text('Gps Truck'),
        'mileage_start': fields.integer('Mileage Start'),
        'mileage_end': fields.integer('Mileage End'),
        'notes': fields.text('Notes'),
        
        #Goods
	    'quantity_perished': fields.integer('Quantity Perished'),
	    'unit_value_perished': fields.integer('Unit Value Perished'),
	    'quantity_repackaged': fields.integer('Quantity Repackaged'),
	    'unit_value_repackaged': fields.integer('Unit Value Repackaged'),
        'goods': fields.many2one('fleet.goods', 'Goods'),
        'goods_location': fields.many2one('fleet.location', 'Goods Location'),
        'packaging_type': fields.many2one('trip.goods.packaging.type',
                                           'Packaging Type'),
        'net_quantity': fields.integer('Net Qty'),
        'uom': fields.many2one('product.uom', 'UOM', domain="['|',('category_id.name', '=', 'Weight'), ('category_id.name', '=', 'Volume')]"),
    }
    
    _rec_name = "trip_name"
    
    _defaults={
        'state':'draft',
    }
    
    def onchange_vehicle(self, cursor, user, ids, vehicle, context=None):
        res = {'value':{}}
        vehicle_obj = self.pool.get('fleet.vehicle')
        if vehicle:
            vehicle = vehicle_obj.browse(cursor, user, vehicle, context=context)
            res['value']['driver_id']=vehicle.driver_id.id
        return res
    
    def write_vehicle(self, cursor, user, trip_ids, vehicle_state, context):
        '''Write the state for truck and associated trailer
        '''
        vehicle_obj = self.pool.get('fleet.vehicle')
        trips = self.browse(cursor, user, trip_ids, context=context)
        vehicle_ids = []
        for trip in trips:
            vehicle_ids.append(trip.truck.id)
            vehicle_ids.append(trip.trailer.id)
            print vehicle_ids
            print vehicle_state
            vehicle_obj.write(cursor, user, vehicle_ids,
                              {'state':vehicle_state}, context=context)
        return True
            
        
    
    def start_trip(self, cursor, user, ids, context=None):
        self.write_vehicle(cursor, user, ids, 'running', context)
        self.write(cursor, user, ids, {'state':'inprocess'}, context=context)
        return True
        
#    def update_locn(self, cursor, user, ids, context=None):
#        print "Update Location"
#        return True
        
    def confirm(self, cursor, user, ids, context=None):
        vals={}
        vals['state']='confirm'
        self.write_vehicle(cursor, user, ids, 'booked', context)
        self.write(cursor, user, ids, vals, context=context)
        return True
        
    def report_breakdown(self, cursor, user, ids, context=None):
        vals={}
        vals['state']='breakdown'
        self.write_vehicle(cursor, user, ids, 'breakdown', context)
        self.write(cursor, user, ids, vals, context=context)
        return True
        
    def resume_trip(self, cursor, user, ids, context=None):
        vals={}
        self.write_vehicle(cursor, user, ids, 'running', context)
        vals['state']='inprocess'
        self.write(cursor, user, ids, vals, context=context)
        return True
        
    def complete(self, cursor, user, ids, context=None):
        vals={}
        vals['state']='complete'
        self.write_vehicle(cursor, user, ids, 'ideal', context)
        self.write(cursor, user, ids, vals, context=context)
        return True
        
    def cancel(self, cursor, user, ids, context=None):
        vals={}
        vals['state']='cancel'
        # Lets call a wizard to check if the trip is cancelled due to serious 
        # breakdown, or cancelled before start of trip or any other reason. This
        # will effect the state of vehicle as well.
        self.write(cursor, user, ids, vals, context=context)
        return True        
TripManagement()

class FleetLocationType(osv.osv):
    _name = "fleet.location.type"
    _description = "Type of a Location. Can be a Halt, Customs Check point, etc"
    _columns = {
        'name': fields.char('Name', size=40),
    }


class TripLocation(osv.osv):
    _name = 'fleet.location'
    _description = 'Provides the loaction of vehicle'
    
    _columns = {
        'name': fields.char('Name', size=50, required=True),
        'region': fields.char('Region', size=50, required=True),
        'latitude': fields.char('Latitude'),
        'longitude': fields.char('Longitude'),
        'country': fields.many2one('res.country', 'Country', required=True),
        'place_type': fields.many2one('fleet.location.type', 'Type', 
            required=True),
    }
TripLocation()    


class Route(osv.osv):
    _name = 'fleet.route'
    _description = 'Describe Route of Vehicle'
    
    def _get_distance(self, cursor, user, ids, field_name, arg, context=None):
        '''Calculate the net distance based on sub route or routes by location
        '''
        res={}
        routes = self.browse(cursor, user, ids, context=context)
        for route in routes:
            net_distance = 0
            print ids
            if route.by_location:
                for location in route.route_by_location:
                    net_distance += location.distance
                res[route.id] = net_distance
            else: 
                for subroute in route.route_by_route:
                   net_distance += subroute.distance_subroute
            res[route.id] = net_distance
        return res


    _columns = {
        'name': fields.char('Name'),
        'source_id': fields.many2one('fleet.location', 'Source', required=True),
        'destination_id': fields.many2one('fleet.location', 'Destination', required=True),
        'distance': fields.function(_get_distance, type='float', string='Distance'),
        'distance_unit': fields.selection([
                              ('kilometers', 'Kilometers'),
                              ('miles','Miles')
                         ], 'Unit', required=True),
        'route_by_location': fields.one2many('fleet.route.location', 'route', 'Route by Location'),
        'route_by_route': fields.one2many('fleet.route.route', 'route', 'Route by Route'),
        'by_location': fields.boolean('By Location'),
    }
    

   
    _defaults = {
        'name': '/',
        'distance_unit': 'kilometers',
        'by_location': False
    }
    
    def create(self, cursor, user, vals, context=None):
        '''
        '''
        location_obj = self.pool.get('fleet.location')
        source_id = vals['source_id']
        destination_id = vals['destination_id']
        source = location_obj.browse(cursor, user, source_id, context=context)
        destination = location_obj.browse(cursor, user, destination_id, context=context)
        name = str(source.name) + ' to ' + str(destination.name)
        vals['name'] = name
        return super(Route, self).create(cursor, user, vals, context=context)
Route()


class SubRoute(osv.osv):
    _name = 'fleet.route.route'
    _description = 'Describes Subroutes'
    _columns = {
        'subroute': fields.many2one('fleet.route', 'SubRoute'),
        'route': fields.many2one('fleet.route', 'Route'),
        'distance_subroute': fields.float('Distance')
    }
    
    def onchange_subroute(self, cursor, user, ids, part, context=None):
        res = {'value' : {'distance_subroute' : 0}}
        route_obj = self.pool.get('fleet.route')
        route = route_obj.browse(cursor, user, part, context=context)
        print route
        distance = route.distance
        print distance
        res['value']['distance_subroute'] = distance
        print res
        return res       
SubRoute() 


class RouteLocation(osv.osv):
    _name = 'fleet.route.location'
    _description = 'Describes routes by location'
    
    def _get_default_source(self, cursor, user, context):
        print context
    
    _columns = {
        'source': fields.many2one('fleet.location', 'Point A'),
        'destination': fields.many2one('fleet.location', 'Point B'),
        'distance': fields.float('Distance'),
        # Unit of Measure
        'route': fields.many2one('fleet.route', 'Route'),
    }
    
    _defaults = {
        'source':_get_default_source,
    }
RouteLocation() 


class TripGoods(osv.osv):
    _name = "fleet.goods"
    
    _columns = {
        'goods_name': fields.char("Package Id"),
        'quntity_tons': fields.integer("Quntity"),
        'quantity_miss': fields.integer("Quantity Miss"),        
        'unit_quantity': fields.integer("Unit Quantity"),
        'unit_value_miss': fields.integer("Unit Value Miss"),
        #'driver_id': fields.
        #'courier_id': fields.
        'quantity_exceed': fields.integer("Quantity Exceed"),
        'unit_value_exceed': fields.integer("Unit value Exceed"),
        'quantity_miss_total': fields.integer('Quantity Miss Total'),
        'quantity_miss_total_driver': fields.integer('Quantity Miss Total Driver'),
        'unit_value_miss_driver': fields.integer('Unit value Miss Driver'),
        'bags_repackaging': fields.integer('Bags Repackaging'),
        'product_qty_repackaging': fields.integer('Product Qty Repackaging'),    
        
    }
TripGoods()


class TripGoodsPackagingType(osv.osv):
    _name =  'trip.goods.packaging.type'
    
    _columns = {
        'name': fields.char("Packaging Type"),
    }
TripGoodsPackagingType()


class TripInvoiceType(osv.osv):
    _name = 'trip.invoice.type'
    
    _columns = {
        'name': fields.char('Invoice Type'),
    }
TripInvoiceType()
