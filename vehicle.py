from openerp.tools.translate import _
from osv import fields, osv

class FleetVehicle(osv.osv):
    _inherit = 'fleet.vehicle'

    def _vehicle_name_get_fnc(self, cursor, user, ids, prop, unknow_none, 
                              context=None):
        res = {}
        for record in self.browse(cursor, user, ids, context=context):
            if record.type == 'truck':
                res[record.id] = record.model_id.brand_id.name + '/' + \
                                 record.model_id.modelname + ' / ' + \
                                 record.license_plate
            elif record.type == 'trailer':
                res[record.id] = record.trailer_type.name + ' / ' + \
                                 record.license_plate
        return res

    _columns = {
        'name': fields.function(_vehicle_name_get_fnc, type="char", 
                                string='Name', store=True
        ),
    
        # Fields common to both Truck and Trailers
        'note': fields.text('Notes'),
        'entry_into_service_date': fields.date('Entry into Service'),
        'registration_expiration': fields.date('Registration Expiry'),
        'code': fields.char('Code',size=100),
        'active': fields.boolean('Active'),
        'type': fields.selection([
            ('truck','Truck'),
            ('trailer','Trailer'),
            ],'Type', required=True),

    
        # Columns for Truck
        'drivers': fields.one2many('fleet.vehicle.driver','vehicle_id',
            'Drivers', readonly=True),
        'trailers': fields.one2many('fleet.truck.trailer','truck_id',
            'Trailers', readonly=True),
        'trailer_id': fields.many2one('fleet.vehicle', 'Trailer', 
            domain=[('type','=','trailer')]),
        'model_id': fields.many2one('fleet.vehicle.model', 'Model', 
            help='Model of the vehicle'
        ),
        
        # Columns for Trailers
        'trailer_type': fields.many2one('fleet.vehicle.trailer.type', 'Type', 
            help='Type of Trailer'
        ),
        'no_of_tyres': fields.integer('No. of Tyres', help='Number of tyres in the vehicle'),
        'loading_capacity': fields.float('Loading Capacity', help='Max loading capacity'),
        'loading_capacity_uom': fields.many2one('product.uom', 
            'Loading Capacity Unit',  
            domain="['|',('category_id.name', '=', 'Weight')," \
                    "('category_id.name', '=', 'Volume')]"),
        'height': fields.float('Height', help='Height (in meters)'),
        'trucks': fields.one2many('fleet.truck.trailer','trailer_id','Trucks', 
            readonly=True),
        
        'current_load': fields.float('Curent Load', help='Load contained in the Trailer'),
        'goods_contained': fields.char('Goods Carried', size=100),
        
        #Changes in existing model
        'license_plate': fields.char('Registration Number', 
            size=32, required=True, help='Registration number of the vehicle'),
        'driver_id': fields.many2one('hr.employee', 'Driver', 
            help='Driver of the vehicle'),
        'location': fields.many2one('fleet.location', 'Location', 
            help="Current location of the vehicle", readonly=True),
        
        'state': fields.selection([
                ('ideal','Ideal'),
                ('booked','Booked'),
                ('running','Running'),
                ('breakdown','Breakdown'),
                ('servicing','Servicing'),
                ('discard','Discard'),
            ],'Status'),
    }
    
    _defaults = {
        'type':'truck',
        'active':True,
        'state': 'ideal',
    }
    
    def write(self, cursor, user, ids, vals, context=None):
        """
        This function is changed according to the changes made by Py Solutions
        """
        print vals
        for vehicle in self.browse(cursor, user, ids, context):
            changes = []
            if 'model_id' in vals and vehicle.model_id.id != vals['model_id']:
                value = self.pool.get('fleet.vehicle.model').browse(cursor,user,vals['model_id'],context=context).name
                oldmodel = vehicle.model_id.name or _('None')
                changes.append(_("Model: from '%s' to '%s'") %(oldmodel, value))
            if 'driver_id' in vals and vehicle.driver_id.id != vals['driver_id']:
                value = self.pool.get('hr.employee').browse(cursor,user,vals['driver_id'],context=context).name
                olddriver = (vehicle.driver_id.name) or _('None')
                changes.append(_("Driver: from '%s' to '%s'") %(olddriver, value))
                # TODO: Create a new record in fleet.truck.driver with today as start date
                # Update the previous record with today as end date
                
            if 'state_id' in vals and vehicle.state_id.id != vals['state_id']:
                value = self.pool.get('fleet.vehicle.state').browse(cursor,user,vals['state_id'],context=context).name
                oldstate = vehicle.state_id.name or _('None')
                changes.append(_("State: from '%s' to '%s'") %(oldstate, value))
            if 'license_plate' in vals and vehicle.license_plate != vals['license_plate']:
                old_license_plate = vehicle.license_plate or _('None')
                changes.append(_("License Plate: from '%s' to '%s'") %(old_license_plate, vals['license_plate']))
            
            if 'trailer_id' in vals and \
                vehicle.trailer_id.id != vals['trailer_id'] and \
                vals['trailer_id']:
                # TODO: Create a new record in fleet.truck.trailer with today as start date
                # Update the previous record with today as end date
                truck_trailer_obj = self.pool.get('fleet.truck.trailer')
                truck_trailer_vals = {
                    'truck_id' : vehicle.id,
                    'trailer_id' : vals['trailer_id'],
                    'start_date' : fields.date.context_today(self, cursor, user, context=context)
                }
                new_id = truck_trailer_obj.create(cursor, user, truck_trailer_vals, context=context)
                print new_id
                
                #truck_trailer_ids = truck_trailer_obj.search(cursor, user,
                #    [('id','<',new_id), ('truck_id','=',vehicle_id)]

            if len(changes) > 0:
               self.message_post(cursor, user, [vehicle.id], body=", ".join(changes), context=context)

        vehicle_id = super(FleetVehicle,self).write(cursor, user, ids, vals, context)
        return True

FleetVehicle()


class VehicleDriver(osv.osv):
    _name = "fleet.vehicle.driver"
    _columns = {
        'vehicle_id': fields.many2one('fleet.vehicle', 'Vehicle', help='vehicle info'),
        'driver_id': fields.many2one('hr.employee', 'Driver', help='Driver of the vehicle'),
        'start_date': fields.date('Start Date'),
        'end_date': fields.date('End Date'),
        'archived': fields.boolean('Active'),
        'notes': fields.text('Notes'),

    }
VehicleDriver()

class TruckTrailer(osv.osv):
    _name = "fleet.truck.trailer"
    _columns = {
        'truck_id': fields.many2one('fleet.vehicle', 'Truck', domain=[('type','=','truck')]),
        'trailer_id': fields.many2one('fleet.vehicle', 'Trailer', domain=[('type','=','trailer')]),
        'start_date': fields.date('Start Date'),
        'end_date': fields.date('End Date'),
    }
TruckTrailer()

class FleetVehicleTrailerType(osv.osv):
    _name = "fleet.vehicle.trailer.type"
    _columns = {
        'name': fields.char('Name', required=True),
    }
